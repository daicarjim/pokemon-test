import { signOut } from '@lib/firebase';
import { useAuth } from '@contexts/auth';
import styles from './Layout.module.scss';

const Layout = ({ children }) => {
  const [user] = useAuth();

  return (
    <div className={styles.Layout}>
      <nav>
        <span>
          <a href="/">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/2560px-International_Pok%C3%A9mon_logo.svg.png" alt="Logo-Pokemón" width="200" height="50"/>
          </a>
        </span>
        {user && (
        <span>
          <a href="/">
            <button>Pokémon List</button>
          </a> 
        <span>
          <a href="/create">
            <button>Create Pokémon</button>
          </a>
          <span>
            <button onClick={() => signOut()}>Sign Out</button>
          </span>
        </span>
      </span>
        )}
         {user == null && (
         <span>
            <a href="/">
              <button>Pokémon List</button>
            </a>
          <span>
            <a href="/signin">
              <button>Sign In</button>
            </a>
          </span>
          </span> 
        )}
      </nav>
      <main>{children}</main>
    </div>
  );
};

export default Layout;
