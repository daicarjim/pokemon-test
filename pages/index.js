// This component represents the index page for the site. You
// can read more about Pages in the Next.js docs at:
// https://nextjs.org/docs/basic-features/pages

import { getPosts } from '@lib/firebase';
import { getFormattedDate } from '@lib/utils';
import { Layout } from '@components';
import styles from '@styles/index.module.scss';
import Search from 'components/Search/Search';
import Footer from 'components/Footer/Footer';
import React, { useEffect } from "react";


const HomePage = ({ posts }) => {

{/*   

  useEffect(() => { 

  window.OneSignal = window.OneSignal || [];
  OneSignal.push(function() {
    OneSignal.init({
      appId: "b706c571-cd8c-447a-8d63-e1e6b5064cbd",
      safari_web_id: "",
      notifyButton: {
        enable: true,
      },
      allowLocalhostAsSecureOrigin: true,
    });
  });
  
  return () => {
   window.Onesignal = undefined;
 };

},[])
*/}

return (

   <Layout>
     <Search />
    <ul className={styles.HomePage}>
      {posts.map((post) => (
        <li key={post.slug}>
          <a href={`/post/${post.slug}`}>
            <img src={post.coverImage} alt={post.coverImageAlt} /> 
          </a>
         
          <div>

             <h2>{post.title}</h2>

            <span>{getFormattedDate(post.dateCreated)}</span>

            <p
              dangerouslySetInnerHTML={{
                __html: `${post.content.substring(0, 70)}...`,
              }}
            ></p>
            <a href={`/post/${post.slug}`}>View more</a>

          </div>
        </li>
      ))}
    </ul>
    <br />
    <br />
   <Footer />
   </Layout>
)};

export async function getServerSideProps() {
  const posts = await getPosts();

  return {
    props: {
      posts,
    },
  };
}

export default HomePage;
