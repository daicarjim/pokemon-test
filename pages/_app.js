// This is called a "Custom `App` in Next.js.
// We use it for global styles and will later use it for loading an
// icon library. You can read more about this in the Next.js docs at:
// https://nextjs.org/docs/advanced-features/custom-app

import { AuthProvider } from '@contexts/auth';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import '@styles/global.scss';
import Head from 'next/head';
import Script from 'next/script'


library.add(fas);

const App = ({ Component, pageProps }) => (
  <>
  <Head>
     <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
     <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
     <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
     <link rel="manifest" href="/site.webmanifest" />
     <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@algolia/algoliasearch-netlify-frontend@1/dist/algoliasearchNetlify.css" />
     <meta name="theme-color" content="#ffffff" />
  </Head>
    <Script strategy="beforeInteractive" src={`https://cdn.jsdelivr.net/npm/@algolia/algoliasearch-netlify-frontend@1/dist/algoliasearchNetlify.js`}  />
    <Script strategy='lazyOnload'>
    {`
      algoliasearchNetlify({
        appId: 'LJI7RZXIEX',
        apiKey: '06014657c4e7a1be73298a67833e8266',
        siteId: '9eebe3bc-b121-464e-9c43-1380194dfbcd',
        branch: 'master',
        selector: 'div#search',
      })
   `}
    </Script>
    <Script strategy="beforeInteractive" src={`https://cdn.onesignal.com/sdks/OneSignalSDK.js`} async="" />
    <Script strategy='lazyOnload'>
    {`
       window.OneSignal = window.OneSignal || [];
       OneSignal.push(function() {
       OneSignal.init({
       appId: "b706c571-cd8c-447a-8d63-e1e6b5064cbd",
       safari_web_id: "web.onesignal.auto.4132c962-fb08-4b74-acbc-06682d034170",
       notifyButton: {
       enable: true,
      },
     });
   });
   `}
    </Script>

  <AuthProvider>
      <Component {...pageProps} />
  </AuthProvider>
  </>
);

export default App;
